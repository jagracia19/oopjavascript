export let fleet = [
  {
    licenseNum: 'ABC123',
    type: 'drone',
    model: 'amazon 1250',
    airTimeHours: '6050',
    base: 'New York',
    latLong: '40.775596 -73.974615'
  },
  {
    licenseNum: 'XYZ456',
    type: 'drone',
    model: 'amazon 1550',
    airTimeHours: '2100',
    base: 'New York',
    latLong: '40.771956 -73.978531'
  },
  {
    licenseNum: 'AT9900',
    type: 'car',
    make: 'Tesla',
    model: 'Quick Transport',
    miles: '15600',
    latLong: '40.773272 -73.968875'
  }
];
