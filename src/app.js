import $ from 'jquery';
import {Car} from './classes/car.js';
import {Drone} from './classes/drone.js';
import {fleet} from './fleet-data.js';
import {FleetDataService} from './services/fleet-data-service.js';
import {Button} from './ui/button.js';
import {Image} from './ui/image.js';
//import {TitleBar} from './ui/title-bar.js';
import {Menu} from './ui/menu.js';

console.log('in app.js');

let b = new Button('Click me');
b.appendToElement($('body'));

let i = new Image('../images/drone.jpg');
i.appendToElement($('body'));

let m = new Menu('Click me');
m.appendToElement($('body'));

//let titleBar = new TitleBar('Vehicles');

//let b = new Button('Click me');
//b.appendToElement($('body'));

//let titleBar = new TitleBar('Click me 2');
//titleBar.appendToElement($('body'));
//titleBar.appendToElement($('#my-div'));

console.log('at the end');
