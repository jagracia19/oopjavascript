import {Vehicle} from './vehicle.js'

export class Drone extends Vehicle {

  constructor(licenseNum, model, latLong) {
    super(licenseNum, model, latLong);
    this.airTimeHours = null;
    this.base = null;    
  }

}
