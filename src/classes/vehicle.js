export class Vehicle {

  constructor(licenseNum, model, latLong) {
    this.licenseNum = licenseNum;
    this.model = model;
    this.latLong = latLong;
  }

  start() {
    console.log('starting vehicle');
  }

  static getCompanyName() {
    console.log('My company');
  }
}
