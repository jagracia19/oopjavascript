import {Vehicle} from './vehicle.js'

export class Car extends Vehicle {

  constructor(licenseNum, model, latLong) {
    super(licenseNum, model, latLong);
    this.miles = null;
    this.make = null;    
  }

  start() {
    super.start();
    console.log('starting car');
  }

  static getCompanyName() {
    super.getCompanyName();
    console.log('My other company');
  }
}
